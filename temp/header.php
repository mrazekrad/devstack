
<!DOCTYPE html>
<html lang="cs">
<head>
    <!-- Meta, Title -->
    <meta charset="UTF-8">
    <meta name="author" content="" />
    <meta name="description" content="%DESCRIPTION%" />
    <meta name="robots" content="index,follow">    
    <title>%TITLE%</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Style, Fonts -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>