


//burger
$(document).ready(function () {


  $(".navbar-toggler").dblclick(function (e) {
      if (!$(this).is(':animated')) {
          $(".btn-menu").toggleClass("open");
      }
  });

  $(".navbar-toggler").click(function (e) {
      if (!$(this).is(':animated')) {
          $(".btn-menu").toggleClass("open");
      }
  });

});

//end burger


//nová pozice

$(document).ready(function () {
  $(".card-header-new .col-md-6").addClass("nova-pozice");
});

//nová pozice

//změna plus na minus o kariera.php
$(document).ready(function () {


  $(".btn--link").click(function (e) {
      $(this).toggleClass("btn--link__minus");   
      $(".btn--link").not($(this)).removeClass("btn--link__minus");
    
  });

});
//změna plus na minus o kariéry


// hide menu, whne you scrool down
const navbar = document.querySelector('nav');
const fixednavbar = document.querySelector('.contactFixedTop');

let lastScrollTop = 0;
$(window).scroll(function(event){
 var st = $(this).scrollTop();
 if (st > 500 && st > lastScrollTop){
      navbar.classList.add('hide');
      fixednavbar.classList.add('hide');
 } else {
      navbar.classList.remove('hide');
      fixednavbar.classList.remove('hide');
 }
 lastScrollTop = st;
});
//end hide menu




//plynulé posunutí na odkaz
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a.link-move").on('click', function(event) {

    $("#navbarSupportedContent").removeClass("show");

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});